# avc-docker-nexus

Docker image: avcompris/nexus

Usage:

	$ docker run -d --name nexus \
		--ulimit nofile=65536:65536 \
		-v /data/nexus:/nexus-data \
		-e MUNIN_NODE_NAME=sample-nexus.avcompris.com \
		-e MUNIN_NODE_ALLOW=^172\\.17\\.0\\..+$ \
		avcompris/nexus

Exposed port is 8081.

Notes:

* /nexus-data must have owner `200`

See the original
[Dockerfile](https://github.com/sonatype/docker-nexus3/blob/master/Dockerfile)