# File: avc-docker-nexus/Dockerfile
#
# Use to build the image: avcompris/nexus, which is Nexus3 + munin-node

FROM sonatype/nexus3:3.49.0
MAINTAINER david.andriana@avantage-compris.com

#-------------------------------------------------------------------------------
# 8. BUILDINFO (RELY ON JENKINS: POPULATED VIA "buildinfo.sh")
#-------------------------------------------------------------------------------

COPY buildinfo /

#-------------------------------------------------------------------------------
#   9. END
#-------------------------------------------------------------------------------





